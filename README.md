# Datascience Projekt zur Vorhersage des Grundwasserspiegels mit CNNs
Forschungsprojekt im Rahmen eines studienbegleitenden Praktikums bei der Bundesanstalt für Geowissenschaften und Rohstoffe (BGR).


Ziel des Projekts ist die Vorhersage des Grundwasserspiegels an unregelmäßig verteilten Messstellen in Hessen mittels eines CNNs. Dazu werden zeitabhängige Faktoren wie Niederschlag und Temperatur genutzt.


Aktueller Fortschritt, weiterer Plan und Ideen können in den Issues nachvollzogen werden. Dort können auch weitere Vorschläge / Anmerkungen etc. hinzugefügt werden.