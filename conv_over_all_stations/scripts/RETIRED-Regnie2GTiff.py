'''
Regnie2GTiff
============
Umwandeln von Regnie Datensätzen in GeoTiff. Script ist mit Reprojektion noch deutlich zu langsam! (ca. 20s pro Datei auf meinem Laptop)

entsprechende Terminal Befehle
------------------------------
csv in shp file
mit reprojektion in UTM 32N von WGS84 Lat/Lon
>ogr2ogr -s_srs EPSG:4326 -t_srs EPSG:32632 -oo X_POSSIBLE_NAMES=LON -oo Y_POSSIBLE_NAMES=LAT  -f "ESRI Shapefile" test.shp test.csv
ohne reprojektion
>ogr2ogr -oo X_POSSIBLE_NAMES=LON -oo Y_POSSIBLE_NAMES=LAT  -f "ESRI Shapefile" test.shp test.csv

Pixelsize von 0.1 Grad (Für EPSG:4326)
>gdal_rasterize -a VAL -tr 0.1 0.1 -a_nodata 0.0 -te 5.866667 47.275 15.033333 55.058333 -ot Float32 -of GTiff test.shp test_raster.tif
Pixelsize von 1000m (wenn EPSG:32632 / UTM 32N)
>gdal_rasterize -a VAL -tr 1000 1000 -a_nodata 0.0 -ot Float32 -of GTiff test.shp test_raster_repro.tif
nur Messgebiet
>gdal_rasterize -a VAL -tr 1000 1000 -a_nodata 0.0 -te 414399 5485664 580244 5719251 -ot Float32 -of GTiff test.shp test_raster_hessen.tif

Extents Messstationen
---------------------
in EPSG:4326
xmin, ymin, xmax, ymax
7.8044554377607795, 49.52353865245542, 10.144771850672523, 51.623332230050934

in EPSG:32632
xmin, ymin, xmax, ymax
414399, 5485664, 580244, 5719251
'''

import os
from osgeo import gdal, ogr
import lib.ogr2ogr
import lib.Regnie2CSV
import time #nur für Benchmark
start_time = time.time() #Benchmark

#für Testzwecke
src_path = r'D:/GruV-Net/data/precipitation/test/'
dest_path = r'D:/GruV-Net/data/precipitation/test/'
#fname = 'ra000107'
fname = 'ra181206'

#raw Regnie Daten in CSV umwandeln
lib.Regnie2CSV.create_full_csv(src_path+fname, src_path+fname+'.csv')

print("--- Regnie2CSV %s seconds ---" % (time.time() - start_time)) #Benchmark

#vrt zu der csv erstellen, geht hoffentlich noch besser ohne schreiben auf disk
vrt = open(src_path+'regnie.vrt', 'w')
vrt_str = r'<OGRVRTDataSource><OGRVRTLayer name="'+fname+r'"><SrcDataSource>'+src_path+fname+'.csv' + r'</SrcDataSource><SrcLayer>'+fname+r'</SrcLayer><GeometryType>wkbPoint</GeometryType><LayerSRS>WGS84</LayerSRS><GeometryField encoding="PointFromColumns" x="LON" y="LAT"/></OGRVRTLayer></OGRVRTDataSource>'
vrt.write(vrt_str)
vrt.close()

print("--- VRT %s seconds ---" % (time.time() - start_time)) #Benchmark

#diese Zeile braucht verdammt viel Zeit!
#csv reprojezieren in UTM 32N und umwandeln in ein Shapefile
#lib.ogr2ogr.main(['', '-s_srs', 'EPSG:4326', '-t_srs', 'EPSG:32632', '-f', 'ESRI Shapefile', dest_path+'out.shp', src_path+'regnie.vrt'])

print("--- ogr2ogr %s seconds ---" % (time.time() - start_time)) #Benchmark

'''
mit vorheriger Reprojektion in UTM 32N
--------------------------------------
Optionen für Rasterrize
xRes, yRes 1000 für 1km Raster in UTM32N
outputBounds -- Gebiet in dem sich die Messstationen befinden, muss noch um Pufferzone ergänzt werden
'''
#OPTIONS = gdal.RasterizeOptions(attribute='VAL', xRes=1000, yRes=1000, noData=0.0, format='GTiff', outputBounds=[414399, 5485664, 580244, 5719251])
#gdal.Rasterize(dest_path+fname+'_hessen.tif', dest_path+'out.shp', options=OPTIONS)

'''
ohne vorheriger Reprojektion; deutlich schneller
------------------------------------------------
Optionen für Rasterrize
Regnie Zellen: 1 Längengrad (x) setzt sich aus 60 Gitterpunkten, 1 Breitengrad (y) aus 120 Gitterpunkten zusammen.
-> xRes = 0.01666, yRes = 0,00833
outputBounds -- Gebiet in dem sich die Messstationen befinden, muss noch um Pufferzone ergänzt werden
'''
OPTIONS = gdal.RasterizeOptions(attribute='VAL', xRes=0.016667, yRes=0.008334, noData=0.0, format='GTiff', outputBounds=[7.8044554377607795, 49.52353865245542, 10.144771850672523, 51.623332230050934])
gdal.Rasterize(dest_path+fname+'_hessen_wgs84.tif', src_path+'regnie.vrt', options=OPTIONS)

print("--- Rasterize %s seconds ---" % (time.time() - start_time)) #Benchmark

#csv löschen
os.remove(src_path+fname+'.csv')

print("--- Total %s seconds ---" % (time.time() - start_time)) #Benchmark