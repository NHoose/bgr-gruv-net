library(tidyverse)
library(raster)
library(data.table)

r <- raster(xmn=414399, xmx=580244, ymn=5485664, ymx=5719251, res=c(1170, 926), crs="+init=epsg:32632 +proj=utm +zone=32 +datum=WGS84 +units=m +no_defs +ellps=WGS84")

source_directory <- "F:/GruV-Net/Data/groundwater_levels_preprocessed_hesse/weekly/filled/csv/"
csv_files <- source_directory %>% list.files()
data <- lapply(source_directory %>% paste0(csv_files), function(x) fread(x, stringsAsFactors = FALSE))

rastered <- data %>% map(function(x) rasterize(x[, c('OSTWERT', 'NORDWERT')], r, x[, 'VAL'], fun=mean, background=0.0))

tif_file_names <- tools::file_path_sans_ext(csv_files) %>% 
  str_c( "F:/GruV-Net/Data/groundwater_levels_preprocessed_hesse/weekly/filled/GTiff/",
         .,
         ".tif")

rastered %>% 
  map2(.x = .,
       .y = tif_file_names, 
       writeRaster, format="GTiff", overwrite=TRUE)