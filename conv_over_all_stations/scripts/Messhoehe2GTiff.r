library(tidyverse)
library(raster)
library(data.table)

r <- raster(xmn=414399, xmx=580244, ymn=5485664, ymx=5719251, res=c(1170, 926), crs="+init=epsg:32632 +proj=utm +zone=32 +datum=WGS84 +units=m +no_defs +ellps=WGS84")
data <- read.csv2("F:/GruV-Net/data/Stammdaten_HE.csv")
raster <-  rasterize(data[, c('OSTWERT', 'NORDWERT')], r, data[, 'MESSPUNKTHOEHE'], fun=mean, background=0.0)
writeRaster(raster, "F:/GruV-Net/data/Messpunkthoehe_raster.tif", format="GTiff", overwrite=TRUE)