library(esmisc)
library(raster)
library(tidyverse)

source_directory <- "D:/GruV-Net/Data/precipitation/raw/"
raw_files <- source_directory %>% list.files()
r <- raster(xmn=414399, xmx=580244, ymn=5485664, ymx=5719251, res=c(1170, 926), crs="+init=epsg:32632 +proj=utm +zone=32 +datum=WGS84 +units=m +no_defs +ellps=WGS84")

for(i in seq(1,32000,1000)) {
  start <- i
  end <- i+999
  raster_files <- source_directory %>% paste0(raw_files[start:end]) %>% map(esmisc::read_regnie)

  raster_files <- raster_files %>% map(projectRaster,to=r)

  tif_file_names <- raw_files[start:end] %>% 
    str_c( "D:/GruV-Net/Data/precipitation/GTiff/",
          .,
          ".tif")

  raster_files %>% 
    map2(.x = .,
        .y = tif_file_names, 
        writeRaster, format="GTiff", overwrite=TRUE)
}

#remaining, geht sicherlich deutlich schoener...
raster_files <- source_directory %>% paste0(raw_files[32001:32142]) %>% map(esmisc::read_regnie)

raster_files <- raster_files %>% map(projectRaster,to=r)

tif_file_names <- raw_files[32001:32142] %>% 
  str_c( "D:/GruV-Net/Data/precipitation/GTiff/",
         .,
         ".tif")

raster_files %>% 
  map2(.x = .,
       .y = tif_file_names, 
       writeRaster, format="GTiff", overwrite=TRUE)