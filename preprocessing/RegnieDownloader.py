import os
import wget

local_path = r'D:/GruV-Net/data/precipitation/'
remote_path = "https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/regnie/"

years = list(range(1931, 2019)) #2019

for y in years:
    fname = "ra"+str(y)+"m.tar"
    url = remote_path + fname
    wget.download(url, os.path.join(local_path, fname))