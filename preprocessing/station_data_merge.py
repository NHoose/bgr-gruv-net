import os
import glob
import pandas as pd

os.chdir("D:/GruV-Net/data/groundwater_levels_preprocessed_hesse")

extension = 'txt'
files = [i for i in glob.glob('*.{}'.format(extension))]

merged = pd.concat([pd.read_csv(f) for f in files ])
merged.to_csv( "merged_data_HE.csv", index=False, encoding='utf-8-sig')