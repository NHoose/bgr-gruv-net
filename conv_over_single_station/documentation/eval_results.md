# Evaluierung
Zeitraum: 2008-2017, 5 Splits, 6 Folds
Metrik: RMSE

Eingabe: Niederschlag + Temperatur

|Split|xgboost|CNN|CNN 1hot|CNN raster mean|oA Mean|Station Mean|Previous|alway 0|
|-|-|-|-|-|-|-|-|-|
|1|0.5609|0.5612|0.5653|0.5768|0.5344|0.3581|0.7865|0.5338
|2|0.5067|0.5143|0.5158|0.5424|0.5187|0.3403|0.7801|0.5185
|3|0.5012|0.508|0.5101|0.5240|0.5389|0.3420|0.8834|0.5390
|4|0.5362|0.5256|0.5382|0.5709|0.5485|0.3669|0.8114|0.5488
|5|0.5326|0.538|0.5347|0.5549|0.5535|0.3365|0.8781|0.5534

tensorboard: https://tensorboard.dev/experiment/Fz8F0BpASmm7xkW8ZBxyWw/


|Split|CNN single station ('HE_10025')|
|-|-|
|1|0.1495|
|2|0.1184|
|3|0.0797|
|4|0.1559|
|5|0.1247|