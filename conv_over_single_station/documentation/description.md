# GruV-Net v2 - Convolution over single Stations

Ziel ist die Vorhersage des Grundwasserspiegels pro Station mit einem Modell, welches auf allen Stationen trainiert wurde.

**Eingabe:**
- Raster um die Messstation mit räumlichen Parametern
    - Niederschlagsdaten
    - Später zusätzlich möglich: NDVI, Landcover, Gelände-Slope, andere Messstationen im Raster
- Numerische Features
    - letzte(r) Messtand(e) der Station (als Differenz zum Timestep davor)
    - später zusätzlich möglich: Temperatur, Stationen One-Hot codiert, ...

**Ausgabe:**
zukünftige(r) Messstand(e) der jeweiligen Station. Interessanter Zeitraum 1-3 Monate.

**Parameter:**
- Rastergröße um Messstation
- Rasterauflösung
- Zeitliche Auflösung Niederschlag (täglich, wöchentlich summiert, monatlich summiert)
- Anzahl vorhergehender Timesteps Niederschlag
- Zeitliche Auflösung Grundwasser Stand
- Anzahl vorhergehender Timesteps Grundwasserstand
- Anzahl und zeitliche Auflösung vorhergesagter Timesteps
- Model Parameter

**Ausgangspunkt:**
- Ausgabe: Veränderung Messstand 4 Wochen nach letztem Messstand zu letztem Messstand
- Eingabe:
    - 11x11 (ca. 11kx11km) Raster wöchentlich summierter Niederschlag für 8 Wochen vor letztem Messstand und vorhergesagter für die 4 Wochen nach letztem Messstand
    - 11x11 monatl. Durchschnittstemperatur für die 3 vorkommenden Monate
    - letzter Messstand und Messstand 4 Wochen davor, jeweils als Veränderung zu 4 Wochen davor