# GruV-Net Evaluation Framework

Daten Zeitraum später: 40 Jahre 1979-2018
zu Beginn: 10 Jahre 2008-2017

Nested Cross-Validation mit Train, Val, Test und Fold-Größe 5 (zu Beginn 2) Jahre

Leave-One-out validation 5% der Stationen (die 5% der Stationen mit der höchsten ID werden weg gelassen)

Metriken pro Fold:
- RSME
- MAE
- Median Absolute Error
- Std. of AE
- trend classification acc. (Stimmen die Vorzeichen überein?)